#include <Ethernet.h>
#include <SPI.h>
#include <ArduinoJson.h>
#include "RestClient.h"

#define delayTime 300     // Time in seconds beetwen sendings
//#define IP "http://cipservidor.cloudapp.net"  // Server IP
#define IP "cipservidor.cloudapp.net"  // Server IP
//#define PORT 5000         // Server Port

//RestClient client = RestClient(IP, PORT);
RestClient client = RestClient(IP);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  // Connect via DHCP
  Serial.println("connect to network");
  client.dhcp();
  
  /*// Can still fall back to manual config:
  byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
  //the IP address for the shield:
  byte ip[] = { 192, 168, 0, 65 };
  Ethernet.begin(mac,ip);*/

  Serial.println("Setup!");
}

String response;

void loop() {
  // put your main code here, to run repeatedly:
  response = "";
  client.setHeader("Accept: application/json");
  int statusCode = client.get("/webservicessalazar/puerta/list/", &response);
  Serial.print("Status code from server: ");
  Serial.println(statusCode);
  Serial.print("Response body from server: ");
  Serial.println(response);
  delay(1000);
}
