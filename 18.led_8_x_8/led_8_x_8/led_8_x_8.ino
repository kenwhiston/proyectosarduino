#include <LedControlMS.h>

#define NumMatrix 1

LedControl lc=LedControl(12,11,10, NumMatrix);

void setup() {
  // put your setup code here, to run once:
  for (int i=0; i< NumMatrix ; i++)
  {
     lc.shutdown(i,false);    // Activar las matrices
     lc.setIntensity(i,8);    // Poner el brillo a un valor intermedio
     lc.clearDisplay(i);      // Y borrar todo
  }

}

void loop() {
  // put your main code here, to run repeatedly:
   lc.writeString(0," CUCHI TE VAS A RECUPERAR PRONTO");
   delay(1000);
}
