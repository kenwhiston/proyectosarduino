//
// Copyright 2015 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// FirebaseRoom_ESP8266 is a sample that demo using multiple sensors
// and actuactor with the FirebaseArduino library.

#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

// Set these to run example.
#define FIREBASE_HOST "iottest-1483c.firebaseio.com" //"project-4261598369233937760.firebaseio.com"
#define FIREBASE_AUTH  "k1dLt0GzfWRfSiTSakPSUvi3QHjYxQrqF7LExa3s" //"JNj8dR9iqW8h1pIWx3LY5GiKq1yIgcXTsy5MRLim"
#define WIFI_SSID "G6_2000"
#define WIFI_PASSWORD "borj@201744939310"
//#define WIFI_SSID "milagritos" 
//"BorjaHouse2018"
//#define WIFI_PASSWORD "19061988"
//"Borj@2@18Ros@l6sG@6l"


const int ledPin = 5;

void setup() {
  Serial.begin(9600);
  
  pinMode(ledPin, OUTPUT);

  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  //Firebase.setInt("led",0);
  
}

int valor = 0;

void loop() {

   valor = Firebase.getInt("led");

   Serial.println(valor);

   if(valor == 1){
      digitalWrite(ledPin, HIGH);
   }else{
      digitalWrite(ledPin, LOW);
   }
  
  
  delay(200);
}
