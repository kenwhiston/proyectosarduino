#include <SoftwareSerial.h>   // Incluimos la librería  SoftwareSerial  
SoftwareSerial BT(10,11);    // Definimos los pines RX y TX del Arduino conectados al Bluetooth

String string;
char command;
#define led 12

void setup()
{
  pinMode(led, OUTPUT);
  BT.begin(9600);       // Inicializamos el puerto serie BT (Para Modo AT 2)
  Serial.begin(9600);   // Inicializamos  el puerto serie  
}
 
void loop()
{
  
  if(BT.available())    // Si llega un dato por el puerto BT se envía al monitor serial
  {

    string = "";
    
    while(BT.available() > 0)
    {
      command = ((byte)BT.read());
      
      if(command == ':')
      {
        break;
      }
      
      else
      {
        string += command;
      }
      
      delay(1);
    }

    //string = BT.read();
    Serial.println(string);

    //verificamos lo obtenido, para encender o apagar el led
    if(string == "TO")
    {
        ledOn();
    }
    
    if(string =="TF")
    {
        ledOff();
    }
  }
  
  if(Serial.available())  // Si llega un dato por el monitor serial se envía al puerto BT
  {
     BT.write(Serial.read());
  }
}


void ledOn()
   {
      digitalWrite(led, HIGH);
      delay(10);
    }
 
 void ledOff()
 {
      digitalWrite(led, LOW);
      delay(10);
 }
