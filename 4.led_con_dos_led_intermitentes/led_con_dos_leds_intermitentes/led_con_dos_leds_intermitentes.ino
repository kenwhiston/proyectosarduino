//------------------------------------
//Función principal
//------------------------------------

// Se ejecuta cada vez que el Arduino se inicia
void setup(){
  pinMode(13,OUTPUT); // Inicializa el pin 13 como una salida 
  pinMode(12,OUTPUT);
}

//------------------------------------
//Función cíclica 
//------------------------------------
void loop() {
  
  // Esta función se mantiene ejecutando
  // cuando este energizado el Arduino 
  digitalWrite(13,HIGH); // Enciende el LED
  digitalWrite(12,LOW);
  delay(100); // Temporiza un segundo (1s = 1000ms) 
  digitalWrite(13,LOW); // Apaga el LED
  digitalWrite(12,HIGH);
  delay(100); // Temporiza un segundo (1s = 1000ms)
 
}
