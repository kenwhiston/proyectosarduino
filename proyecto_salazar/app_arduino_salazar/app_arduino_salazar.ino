#include <Ethernet.h>
#include <SPI.h>
//#include <ArduinoJson.h>
#include "RestClient.h"
#include <Servo.h>

#define IP "cipservidor.cloudapp.net"  // Server IP

#define PUERTA1 2
#define PUERTA2 4
#define PUERTA3 7

#define SERVO1 5
#define SERVO2 6
#define SERVO3 9


RestClient client = RestClient(IP);

Servo mimotor1; //Objeto para el servo1
Servo mimotor2;
Servo mimotor3;

int valorPuerta1, valorPuerta2, valorPuerta3;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  pinMode(PUERTA1, INPUT_PULLUP);

  mimotor1.attach(SERVO1);
  mimotor2.attach(SERVO2);
  mimotor3.attach(SERVO3);

  Serial.println("Conectando a red...!");
  client.dhcp();
  Serial.println("Setup OK!");

}

void loop() {
  // put your main code here, to run repeatedly:
  //leerPuertas();
  //valorPuerta1 = 1;
  //valorPuerta2 = 0;
  //valorPuerta3 = 1;

  //enviarDatos();

  recibirDatos();

  controlarPuertas();

  Serial.println(memoryTest());

  //delete[] &response;

  //Serial.println(memoryTest());
  
  //espera general
  delay(100);

}

void leerPuertas(){

  //byte estadoPuerta1, estadoPuerta2, estadoPuerta3;
  
  //estadoPuerta1 = digitalRead(PUERTA1);
  
  if (digitalRead(PUERTA1) == HIGH) {
    //puerta1 abierta
    Serial.println("puerta1 abierta");
    valorPuerta1 = 0;
  }else{
    //puerta cerrada
    Serial.println("puerta1 cerrada");
    valorPuerta1 = 1;
  }

  //estadoPuerta2 = digitalRead(PUERTA2);
  
  if (digitalRead(PUERTA2) == HIGH) {
    //puerta2 abierta
    Serial.println("puerta2 abierta");
    valorPuerta2 = 0;
  }else{
    //puerta2 cerrada
    Serial.println("puerta2 cerrada");
    valorPuerta2 = 1;
  }


  //estadoPuerta3 = digitalRead(PUERTA3);
  
  if (digitalRead(PUERTA3) == HIGH) {
    //puerta3 abierta
    Serial.println("puerta3 abierta");
    valorPuerta3 = 0;
  }else{
    //puerta3 cerrada
    Serial.println("puerta3 cerrada");
    valorPuerta3 = 1;
  }
  
}


void enviarDatos(){
  //prueba de lanzamiento de peticion get http
  String response;
  String url;
  url = "/webservicessalazar/puerta/update/"+String(valorPuerta1)+"/"+String(valorPuerta2)+"/"+String(valorPuerta3)+"/";

  char str_array[url.length()+1];
  url.toCharArray(str_array, url.length()+1);

  response = "";
  client.setHeader("Accept: application/json");
  int statusCode = client.get(str_array, &response);
  Serial.print("Status code Puertas: ");
  Serial.println(statusCode);
  Serial.print("Response body Puertas: ");
  Serial.println(response);

  delay(200);

  
  /*//enviamos estado de puerta 1
  String response;
  String url;
  url = "/webservicessalazar/puerta/update/Puerta1/"+String(valorPuerta1)+"/";

  char str_array[url.length()+1];
  url.toCharArray(str_array, url.length()+1);

  response = "";
  client.setHeader("Accept: application/json");
  int statusCode = client.get(str_array, &response);
  Serial.print("Status code Puerta1: ");
  Serial.println(statusCode);
  Serial.print("Response body Puerta1: ");
  Serial.println(response);

  delay(200);

  //fin de enviamos estado de puerta 1


  //enviamos estado de puerta 2

  url = "/webservicessalazar/puerta/update/Puerta2/"+String(valorPuerta2)+"/";

  //char str_array[url.length()+1];
  url.toCharArray(str_array, url.length()+1);

  response = "";
  client.setHeader("Accept: application/json");
  statusCode = client.get(str_array, &response);
  Serial.print("Status code Puerta2: ");
  Serial.println(statusCode);
  Serial.print("Response body Puerta2: ");
  Serial.println(response);

  delay(200);

  //fin de enviamos estado de puerta 2

  //enviamos estado de puerta 3

  url = "/webservicessalazar/puerta/update/Puerta3/"+String(valorPuerta3)+"/";

  //char str_array[url.length()+1];
  url.toCharArray(str_array, url.length()+1);

  response = "";
  client.setHeader("Accept: application/json");
  statusCode = client.get(str_array, &response);
  Serial.print("Status code Puerta3: ");
  Serial.println(statusCode);
  Serial.print("Response body Puerta3: ");
  Serial.println(response);

  delay(200);

  //fin de enviamos estado de puerta 3*/
}

void recibirDatos(){

  String response;
  String url;
  String estado;
  int statusCode;

  response = "";
  client.setHeader("Accept: application/json");
  statusCode = client.get("/webservicessalazar/puerta/statusall/", &response);
  Serial.print("Status code Puertas: ");
  Serial.println(statusCode);
  Serial.print("Response body Puertas: ");
  Serial.println(response);

  estado = String(response);

  char uno = estado.charAt(0);
  char dos = estado.charAt(1);
  char tres = estado.charAt(2);
  
  estado =  String(uno);
  valorPuerta1 = estado.toInt();

  Serial.println("Puerta1 - " + String(valorPuerta1));

  estado =  String(dos);
  valorPuerta2 = estado.toInt();

  Serial.println("Puerta2 - " + String(valorPuerta2));

  estado =  String(tres);
  valorPuerta3 = estado.toInt();

  Serial.println("Puerta3 - " + String(valorPuerta3));

  delay(200);

  /*String response;
  String url;
  String estado;
  int statusCode;

  response = "";
  client.setHeader("Accept: application/json");
  statusCode = client.get("/webservicessalazar/puerta/status/Puerta1/", &response);
  Serial.print("Status code Puerta1: ");
  Serial.println(statusCode);
  Serial.print("Response body Puerta1: ");
  Serial.println(response);

  estado = String(response);
  estado.replace("\""," ");
  estado.trim();
  valorPuerta1 = estado.toInt();

  Serial.println("Puerta1 - " + String(valorPuerta1));

  delay(200);

  response = "";
  client.setHeader("Accept: application/json");
  statusCode = client.get("/webservicessalazar/puerta/status/Puerta2/", &response);
  Serial.print("Status code Puerta2: ");
  Serial.println(statusCode);
  Serial.print("Response body Puerta2: ");
  Serial.println(response);

  estado = String(response);
  estado.replace("\""," ");
  estado.trim();
  valorPuerta2 = estado.toInt();

  Serial.println("Puerta2 - " + String(valorPuerta2));

  delay(200);

  response = "";
  client.setHeader("Accept: application/json");
  statusCode = client.get("/webservicessalazar/puerta/status/Puerta3/", &response);
  Serial.print("Status code Puerta3: ");
  Serial.println(statusCode);
  Serial.print("Response body Puerta3: ");
  Serial.println(response);
  
  estado = String(response);
  estado.replace("\""," ");
  estado.trim();
  valorPuerta3 = estado.toInt();

  Serial.println("Puerta3 - " + String(valorPuerta3));

  delay(200);*/
  
  /*//prueba de lanzamiento de peticion get http
  response = "";
  client.setHeader("Accept: application/json");
  int statusCode = client.get("/webservicessalazar/puerta/status/", &response);
  Serial.print("Status code Puertas: ");
  Serial.println(statusCode);
  Serial.print("Response body Puertas: ");
  Serial.println(response);
  
  const size_t bufferSize = 3*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + 110;
  
  DynamicJsonBuffer jsonBuffer(bufferSize);

  //char str_array_res[response.length()+1];
  //response.toCharArray(str_array_res, response.length()+1);
  jsonBuffer.clear();
  
  JsonObject& root = jsonBuffer.parseObject(response);

  if(root.success()) {
    // parseObject() succeeded
    Serial.println("parseObject() succeeded");

    const char* Puerta1_estado = root["Puerta1"]["estado"]; // "0"
    const char* Puerta1_nombre = root["Puerta1"]["nombre"]; // "Puerta1"
  
    const char* Puerta2_estado = root["Puerta2"]["estado"]; // "1"
    const char* Puerta2_nombre = root["Puerta2"]["nombre"]; // "Puerta2"
  
    const char* Puerta3_estado = root["Puerta3"]["estado"]; // "1"
    const char* Puerta3_nombre = root["Puerta3"]["nombre"]; // "Puerta3"

    
    String estado;

    estado = String(Puerta1_estado);

    valorPuerta1 = estado.toInt();

    Serial.println(String(Puerta1_nombre) + " - " + String(valorPuerta1));

    estado = String(Puerta2_estado);

    valorPuerta2 = estado.toInt();

    Serial.println(String(Puerta2_nombre) + " - " + String(valorPuerta2));

    estado = String(Puerta3_estado);

    valorPuerta3 = estado.toInt();

    Serial.println(String(Puerta3_nombre) + " - " + String(valorPuerta3));
    
  }else{
    // parseObject() failed
    Serial.println("parseObject() failed");
  }

 delay(250);*/
  
}

void controlarPuertas(){
  int anguloCerrado=180;
  int anguloAbierto=90;
  //Serial.flush();
  //obtenemos los estados remoto para abrir o cerrar las puertas
  if (valorPuerta1 == 1) {
    //cerramos puerta 1
    mimotor1.write(anguloCerrado);
    Serial.println("puerta1 abierta, entonces cerramos");
    
  }else{
    //abrimos puerta 1
    mimotor1.write(anguloAbierto);
    Serial.println("puerta1 cerrada, entonces abrimos");
  }

  if (valorPuerta2 == 1) {
    //cerramos puerta 2
    mimotor2.write(anguloCerrado);
    Serial.println("puerta2 abierta, entonces cerramos");
    
  }else{
    //abrimos puerta 1
    mimotor2.write(anguloAbierto);
    Serial.println("puerta2 cerrada, entonces abrimos");
  }

  if (valorPuerta3 == 1) {
    //cerramos puerta 3
    mimotor3.write(anguloCerrado);
    Serial.println("puerta3 abierta, entonces cerramos");
    
  }else{
    //abrimos puerta 3
    mimotor3.write(anguloAbierto);
    Serial.println("puerta3 cerrada, entonces abrimos");
  }
}


// this function will return the number of bytes currently free in RAM
int memoryTest() {
  int byteCounter = 0; // initialize a counter
  byte *byteArray; // create a pointer to a byte array
  // More on pointers here: http://en.wikipedia.org/wiki/Pointer#C_pointers

  // use the malloc function to repeatedly attempt allocating a certain number of bytes to memory
  // More on malloc here: http://en.wikipedia.org/wiki/Malloc
  while ( (byteArray = (byte*) malloc (byteCounter * sizeof(byte))) != NULL ) {
    byteCounter++; // if allocation was successful, then up the count for the next try
    free(byteArray); // free memory after allocating it
  }
  
  free(byteArray); // also free memory after the function finishes
  return byteCounter; // send back the highest number of bytes successfully allocated
}

