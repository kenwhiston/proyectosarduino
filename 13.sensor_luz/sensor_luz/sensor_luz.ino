int sensorReading;//Pin análogo en espera
void setup()
{
 Serial.begin(9600);
 pinMode(13,OUTPUT); // 
 pinMode(A0, INPUT);
}
void loop()
{
 sensorReading=analogRead(A0); //Instrucción para obtener dato analógico
 if (sensorReading<700)
 {
  digitalWrite(13,HIGH);
 }else{
  digitalWrite(13,LOW);
 }
 Serial.println(sensorReading); 
 delay(1000);
}
