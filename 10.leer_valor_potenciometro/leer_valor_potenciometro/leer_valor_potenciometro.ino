void setup() {
  Serial.begin(9600); //Inicia comunicación serial
}
void loop() {
  //Guardar en una variable entera el valor del potenciómetro 0 a 1023
  int valor= analogRead(A0);
  //Imprime en la consola serial el valor de la variable
  Serial.println(valor);
  //Retardo para la visualización de datos en la consola
  if (valor >= 500)
  {
    digitalWrite(3,HIGH); //Enciende el LED en el pin 9
  }
  else
  {
    //Si el valor es menor a 500
    digitalWrite(3,LOW); //Apaga el LED en el pin 9
  }
  delay(100); //Retardo de 100ms para ver los datos de la consola
}
