// Se ejecuta cada vez que el Arduino se inicia
void setup(){
  pinMode(12,OUTPUT); // Inicializa el pin 12 como una salida 
}

//------------------------------------
//Función cíclica 
//------------------------------------
void loop() {
  
  // Esta función se mantiene ejecutando
  // cuando este energizado el Arduino 
  digitalWrite(12,HIGH); // Enciende el LED
  delay(500); // Temporiza un segundo (1s = 1000ms) 
  digitalWrite(12,LOW); // Apaga el LED
  delay(500); // Temporiza un segundo (1s = 1000ms)
 
}
