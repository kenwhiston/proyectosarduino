#include <DHT11.h>


int pin=2;
DHT11 dht11(pin);

#include <LiquidCrystal.h>
LiquidCrystal lcd(13,12,11,10,9);



void setup() 
   {
       lcd.begin(16, 2);           // Fijamos el numero de caracteres y filas
       lcd.print("Bitec Arduino");  // Aqi va el mensaje

       Serial.begin(9600);

      
       
   }

void loop()
   {

      lcd.clear();

      //primero ponemos mensaje de bitec
      lcd.setCursor(0,0);
      lcd.print("Bitec Arduino");
    
       lcd.setCursor(0, 1);    // Ponte en la line 1, posicion 6
       //String s = reloj() ;
       //lcd.print(s) ;

      int err;
       float temp, hum;
       if((err = dht11.read(hum, temp)) == 0)    // Si devuelve 0 es que ha leido bien
          {
             Serial.print("Temperatura: ");
             Serial.print(temp);
             Serial.print(" Humedad: ");
             Serial.print(hum);
             Serial.println();

            String s = "H:" + String(hum) + "-T:" +String(temp); 
            lcd.print(s);
          }
       else
          {
             Serial.println();
             Serial.print("Error Num :");
             Serial.print(err);
             Serial.println();
          }


       delay(3000);            //Recordad que solo lee una vez por segundo

      
   }


   
/*String reloj()
   {
       int n = millis() / 1000 ;       // Lo pasamos a segundos
       int segundos = n % 60  ;
       int minutos =  n / 60  ;

       String S = String(minutos) + ":" + String(segundos);
       return (S);
   }*/
