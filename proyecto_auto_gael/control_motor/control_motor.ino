#define e1 10  // Enable Pin for motor 1
#define i1 8   // Control pin 1 for motor 1
#define i2 9   // Control pin 2 for motor 1

void setup()
   {
        for (int i = 8 ; i<11 ; i++)                     
           pinMode( i, OUTPUT);
   }
void loop(){
    digitalWrite(e1, HIGH); // Activamos Motor1
    digitalWrite(i1, HIGH); // Arrancamos
    digitalWrite(i2, LOW);
    delay(3000);

    digitalWrite(e1, LOW);  // Paramos Motor 1
    delay(1000);
    digitalWrite(e1, HIGH); // Activamos Motor1
    digitalWrite(i1, LOW);  //cambio de dirección
    digitalWrite(i2, HIGH);
    delay(3000);

    digitalWrite(e1, LOW);  // Paramos Motor 1
    delay(1000);
  }
