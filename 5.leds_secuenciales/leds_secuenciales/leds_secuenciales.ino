//------------------------------------
//Función principal
//------------------------------------

// Se ejecuta cada vez que el Arduino se inicia
void setup(){
  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT); // Inicializa el pin 13 como una salida 
  pinMode(11,OUTPUT);
  pinMode(12,OUTPUT);
}

//------------------------------------
//Función cíclica 
//------------------------------------
void loop() {
  
  // Esta función se mantiene ejecutando
  // cuando este energizado el Arduino 
  digitalWrite(9,HIGH); // Enciende el LED
  digitalWrite(10,LOW);
  digitalWrite(11,LOW);
  digitalWrite(12,LOW);
  delay(200); // Temporiza un segundo (1s = 1000ms) 
  digitalWrite(9,LOW); // Enciende el LED
  digitalWrite(10,HIGH);
  digitalWrite(11,LOW);
  digitalWrite(12,LOW);
  delay(200); // Temporiza un segundo (1s = 1000ms)
  digitalWrite(9,LOW); // Enciende el LED
  digitalWrite(10,LOW);
  digitalWrite(11,HIGH);
  digitalWrite(12,LOW);
  delay(200);
  digitalWrite(9,LOW); // Enciende el LED
  digitalWrite(10,LOW);
  digitalWrite(11,LOW);
  digitalWrite(12,HIGH);
  delay(200);
 
}
