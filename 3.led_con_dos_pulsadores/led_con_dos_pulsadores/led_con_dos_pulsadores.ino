//------------------------------------
//Función principal
//------------------------------------
//------------------------------------
//Declara puertos de entradas y salidas //------------------------------------
int pulsadorA=2; //Pin donde se encuentra el pulsador, entrada 
int pulsadorB=7; //Pin donde se encuentra el pulsador, entrada 
int led=13; //Pin donde se encuentra el LED, salida

// Se ejecuta cada vez que el Arduino se inicia
void setup(){
  pinMode(pulsadorA, INPUT); //Configurar el pulsador como una entrada
  pinMode(pulsadorB, INPUT); //Configurar el pulsador como una entrada
  pinMode(led,OUTPUT); //Configurar el LED como una salida
}

//------------------------------------
//Función cíclica 
//------------------------------------
void loop() {
  
  // Esta función se mantiene ejecutando
  // cuando este energizado el Arduino 
  //Condicional para saber estado del pulsador
  if (digitalRead(pulsadorA)==LOW && digitalRead(pulsadorB)==LOW)
  {
    //Pulsadores oprimido
    digitalWrite(led,HIGH); //Enciende el LED 
  }
  else
  {
    //Pulsadores NO oprimido
    digitalWrite(led,LOW); //Apaga el LED 
  }
 
}
