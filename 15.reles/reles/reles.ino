/*
  Blink para Arduino
*/
//Pin a utilizar
int LedPin = 9;
 
void setup() {
  pinMode(LedPin, OUTPUT); //modo salida
}
 
void loop() {
  digitalWrite(LedPin, HIGH);   // Enciende el pin 6
  delay(500);                   // Espera medio segundo
  digitalWrite(LedPin, LOW);   // Apaga el pin 6
  delay(500);                   // Espera medio segundo
}
