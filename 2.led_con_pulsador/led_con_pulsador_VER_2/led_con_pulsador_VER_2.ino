//------------------------------------
//Función principal
//------------------------------------
//------------------------------------
//Declara puertos de entradas y salidas //------------------------------------
int pulsador=2; //Pin donde se encuentra el pulsador, entrada 
int led=13; //Pin donde se encuentra el LED, salida
int led1=11; //Pin donde se encuentra el LED, salida

// Se ejecuta cada vez que el Arduino se inicia
void setup(){
  pinMode(pulsador, INPUT); //Configurar el pulsador como una entrada
  pinMode(led,OUTPUT); //Configurar el LED como una salida
  pinMode(led1,OUTPUT);
}

//------------------------------------
//Función cíclica 
//------------------------------------
void loop() {
  
  // Esta función se mantiene ejecutando
  // cuando este energizado el Arduino 
  //Condicional para saber estado del pulsador
    digitalWrite(led,LOW); //Enciende el LED 
    digitalWrite(led1,LOW);
  if (digitalRead(pulsador)==HIGH)
  {
    //Pulsador oprimido
    digitalWrite(led,HIGH); //Enciende el LED 
    digitalWrite(led1,LOW);
  }
  else
  {
    //Pulsador NO oprimido
    digitalWrite(led,LOW); //Apaga el LED 
    digitalWrite(led1,HIGH);
  }
 
}
