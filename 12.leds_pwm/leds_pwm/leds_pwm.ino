int brillo = 0;
int variacion = 25;
int pin = 3;
void setup() {
  // put your setup code here, to run once:
  pinMode(pin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(pin,brillo);
  brillo = brillo + variacion;
  if(brillo == 0 || brillo == 255) 
    variacion = -variacion;
  delay(500);
}
