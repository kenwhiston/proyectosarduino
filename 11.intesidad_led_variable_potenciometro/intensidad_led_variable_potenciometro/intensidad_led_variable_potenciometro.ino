

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Inicia comunicación serial
}

void loop() {
  
  // put your main code here, to run repeatedly:
  //Guardar en una variable entera el valor del potenciómetro 0 a 1024
  int valor= analogRead(A0);
  //Imprime en la consola serial el valor de la variable
  Serial.println(valor);
  //Retardo para la visualización de datos en la consola

  //calcumamos, teniendo en cuenta le regla de tres simple, x = (255 * valor) / 1023

  float x = 0.0;
  x =  (255.0 * ( (float)valor / 1023.0));
  
  int intensidad = 0;

  Serial.println(x);

  intensidad = (int)x;
  
  Serial.println(intensidad);
  
  analogWrite(9, intensidad);
  
  delay(100); //Retardo de 100ms para ver los datos de la consola
}
