
#define HUMIDITY_SENSOR A0

int dryValue = 150;
int wetValue = 700;
int friendlyDryValue = 0;
int friendlyWetValue = 100;
 
void setup() {
  Serial.begin(9600);
}
 
void loop() {
  int humidity;
  int bruto;
  bruto = analogRead(HUMIDITY_SENSOR);
  //Serial.println(bruto);
  Serial.print("Raw: ");
  Serial.print(bruto);
  Serial.print(" | ");
  int friendlyValue = map(bruto, wetValue, dryValue, friendlyDryValue, friendlyWetValue);
  Serial.print("Friendly: ");
  Serial.print(friendlyValue);
  Serial.println("%");
  //humidity = 100-0.0245*analogRead(HUMIDITY_SENSOR);
  //humidity= analogRead(HUMIDITY_SENSOR);
  //humidity = map(humidity,550,10,0,100);
  //Serial.println(humidity);
  delay(500);
}
