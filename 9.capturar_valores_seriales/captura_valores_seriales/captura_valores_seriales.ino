//--------------------------------------------------
//Declara puertos de entradas y salidas y variables //--------------------------------------------------
int led = 7; //Pin donde se encuentra el LED, salida 
char leer; //Variable donde se almacena la letra 
boolean prendido=false; //Estado LED la primera vez, apagado

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Inicia comunicación serial 
  pinMode(led, OUTPUT); //Configurar el LED como una salida
}

void loop() {
  // put your main code here, to run repeatedly:
  //Guardar en una variable el valor de la consola serial
  leer=Serial.read();
  // Si es la letra 'a' y además el LED está apagado
  if ( (leer=='a') && (prendido==false) )
  {
    // Si es la letra 'a' y además el LED está encendido
    digitalWrite(led,HIGH); // Enciende el LED
    prendido=true; // Actualiza el estado del LED 
  }
  else if ( (leer=='a') && (prendido==true) )
  {

    digitalWrite(led,LOW); 
    prendido=false;
    // Apaga el LED
    // Actualiza el estado del LED
  }

}
