#define pulsador 2

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(pulsador, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  int valor = 0;
  valor = digitalRead(pulsador);

  if (valor == HIGH){
    Serial.println("HIGH");
  }else{
    Serial.println("LOW");
  }

  delay(250);
  
}
